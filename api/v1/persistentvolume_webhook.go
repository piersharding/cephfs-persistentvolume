/*

Copyright 2022 Piers Harding
Copyright 2022 SKA Observatory

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/
// +kubebuilder:docs-gen:collapse=Apache License

// Go imports
package v1

import (
	"context"
	"encoding/json"
	"net/http"

	"sigs.k8s.io/controller-runtime/pkg/webhook/admission"

	// Import all Kubernetes client auth plugins (e.g. Azure, GCP, OIDC, etc.)
	// to ensure that exec-entrypoint and run can make use of them.

	_ "k8s.io/client-go/plugin/pkg/client/auth"
	"sigs.k8s.io/controller-runtime/pkg/client"

	//+kubebuilder:scaffold:imports

	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/serializer"
	logf "sigs.k8s.io/controller-runtime/pkg/log"
)

var (
	codecs       = serializer.NewCodecFactory(runtime.NewScheme())
	deserializer = codecs.UniversalDeserializer()
)

type PersistentVolumeAnnotator struct {
	Client  client.Client
	decoder *admission.Decoder
}

// +kubebuilder:webhook:path=/mutate-v1-persistentvolume,mutating=true,failurePolicy=ignore,groups="",resources=persistentvolumes,verbs=create;update;delete,versions=v1,admissionReviewVersions=v1,sideEffects=None,name=mpv.skao.int

// https://github.com/trstringer/kubernetes-mutating-webhook/blob/main/cmd/root.go
func (a *PersistentVolumeAnnotator) Handle(ctx context.Context, req admission.Request) admission.Response {
	log := logf.FromContext(ctx)

	// Decode the PersistentVolume from the AdmissionReview.
	pv := corev1.PersistentVolume{}
	if _, _, err := deserializer.Decode(req.Object.Raw, nil, &pv); err != nil {
		return admission.Errored(http.StatusBadRequest, err)
	}

	// mutate the fields in PersistentVolume
	if pv.Annotations == nil {
		pv.Annotations = map[string]string{}
	}
	pv.Annotations["cephfs-persistentvolume-mutating-admission-webhook"] = "gotcha"
	log.Info("Annotated PersistentVolume: %s/%s", pv.Namespace, pv.Name)

	marshaledPersistentVolume, err := json.Marshal(pv)
	if err != nil {
		return admission.Errored(http.StatusInternalServerError, err)
	}

	// admission.Response
	return admission.PatchResponseFromRaw(req.Object.Raw, marshaledPersistentVolume)
}

func (a *PersistentVolumeAnnotator) InjectDecoder(d *admission.Decoder) error {
	a.decoder = d
	return nil
}
